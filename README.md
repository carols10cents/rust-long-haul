# Rust: In it for the Long Haul

Short link to this repo: [is.gd/rustLH](https://is.gd/rustLH)

Given as an online ACM TechTalk on Nov 1, 2019 and at SecAppDev on 2020-03-13.

[View the slides](https://carols10cents.gitlab.io/rust-long-haul)

## Links in the presentation

* The Rust Programming Language Book
  * [Online](https://doc.rust-lang.org/stable/book/)
  * [Print](https://nostarch.com/rust)

* [Rust in Motion Manning liveVideo series](https://www.manning.com/livevideo/rust-in-motion?a_aid=cnichols&a_bid=6a993c2e)

* [Integer 32](https://integer32.com/), the world's first Rust consultancy

* [Source for miles of rail in the US](https://en.wikipedia.org/wiki/Rail_transportation_in_the_United_States)

* [Source of the brakeman engraving](https://en.wikipedia.org/wiki/File:900801-peckwell-apicnic.jpg)

* [Source of the picture of George Westinghouse](https://commons.wikimedia.org/wiki/File:George_Westinghouse_1884.png)

* [Source of the Westinghouse Air Brakes advertisement](https://commons.wikimedia.org/wiki/File:Westinghouse_Air_Brake_Denver_Zephyr_7_November_1936.jpg)

* [1890 Senate testimony transcript before the Interstate Commerce Committee on air brakes and automatic couplers](https://books.google.com/books?id=-cIuAAAAMAAJ&printsec=frontcover&dq=brakeman&hl=en&sa=X&ved=0ahUKEwjnlcC15o3TAhUn_4MKHdpOCDc4ggEQ6AEIOTAG#v=onepage&q=brakeman&f=false)

* [A Demonstration of Stagefright-like Mistakes](https://www.xda-developers.com/a-demonstration-of-stagefright-like-mistakes/)

* [Microsoft: 70 percent of all security bugs are memory safety issues](https://www.zdnet.com/article/microsoft-70-percent-of-all-security-bugs-are-memory-safety-issues/)

* [IKOS static analyzer from NASA](https://github.com/NASA-SW-VnV/ikos)

* [Safe-C](https://www.safe-c.org/start-en.html)

* [Checked C](https://www.infoworld.com/article/3084424/microsoft-open-sources-a-safer-version-of-c-language.html)

* [Building on an Unsafe Foundation talk](https://youtu.be/rTo2u13lVcQ)

* [The Rustonomicon](https://doc.rust-lang.org/nomicon/)

* [My "Rust out your C" talk](https://github.com/carols10cents/rust-out-your-c-talk)

* [Detailed description of Rust's security guarantees and exceptions](https://github.com/rust-lang/crates.io/issues/868#issuecomment-484693168)

* [Detailed description of Rust Editions](https://rust-lang.github.io/rfcs/2052-epochs.html)

* [Implications of Rewriting a Browser Component in Rust](https://hacks.mozilla.org/2019/02/rewriting-a-browser-component-in-rust/)

* [Amazon's Firecracker](https://firecracker-microvm.github.io/)

* [Google's Fuchsia](https://en.wikipedia.org/wiki/Google_Fuchsia)

* [Facebook's Mononoke](https://github.com/facebookexperimental/mononoke)

* [Microsoft's IoT Edge](https://github.com/Azure/iotedge)

* [Juicero shows what’s wrong with Silicon Valley thinking](https://www.washingtonpost.com/blogs/post-partisan/wp/2017/04/24/juicero-shows-whats-wrong-with-silicon-valley-thinking/?utm_term=.8e8d546d5e3c)

* [The Lyft Shuttle is pretty much a glorified city bus — with fewer poor people](https://www.salon.com/2017/06/19/lyfts-shuttle-is-pretty-much-a-glorified-city-bus-with-fewer-poor-people/)

* [Elon Musk 'can stick his submarine where it hurts' says British diver who helped Thai cave rescue](https://www.telegraph.co.uk/news/2018/07/14/elon-musk-can-stick-submarine-hurts-says-british-diver-helped/)

* [The UK's NHS diverted ambulances and delayed procedures due to the WannaCry ransomware getting on their computer systems](https://www.bbc.com/news/health-39899646)

* [Details on EternalBlue, one of the exploits behind WannaCry and other ransomware, used input validation/buffer overflow to get remote code execution](https://www.virusbulletin.com/virusbulletin/2018/06/eternalblue-prominent-threat-actor-20172018/)

* [Rep. Riggleman questioning David Marcus, Head of Calibra at Facebook, on what features of nightly Rust Libra needs on July 17, 2019](https://youtu.be/9-ZTkCNW0w8?t=13415) ([text transcript](https://riggleman.house.gov/media/press-releases/congressman-denver-riggleman-questions-facebook-cryptocurrency-chief-david))
